# QompliGIS - QGIS plugin

[![pipeline status](https://gitlab.com/Oslandia/qgis/qompligis/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/qompligis/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/qompligis/)
[![pylint](https://oslandia.gitlab.io/qgis/qompligis/lint/pylint.svg)](https://oslandia.gitlab.io/qgis/qompligis/lint/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)


## QompliGIS Plugin

This plugin aims to provide a simple way to verify if the structure of a dataset complies
with the structure of a reference dataset.

The spirit of the plugin is to verify the dataset compliance with:

- a reference dataset
- and a reference configuration specifying various constraints (mandatory field, length limit in a layer...)

To verify a dataset, the dedicated wizard (step-by-step interface assistant) will ask to load the configuration file and the dataset to check.
To create a reference configuration file, the dedicated wizard will ask to load the reference dataset.

![report](docs/_static/images/en/check_1.png)

## Documentation

[:gb: Check-out the documentation - :fr: Consulter la documentation](https://oslandia.gitlab.io/qgis/qompligis/)

## Installation

The plugin can be installed with QGIS plugin manager looking for ["QompliGIS"](https://plugins.qgis.org/plugins/QompliGIS/).

## Further development

We plan to realise some developments in QGIS core soon to enhance the geometrical and topological verification, in order to access it in this plugin.

If you too, you want to contribute enhancing and developping this tool or train yourself on this topic in QGIS, don't hesitate to contact us by [mail](mailto:infos@oslandia.com?subject=%5BQompliGIS%5D%20Request) or opening an issue.
